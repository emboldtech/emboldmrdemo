

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

class ResourceLeak {  
	public void process1() {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("out.txt", true)));
			out.println("the text");
			out.close();  //close() is in try clause

			FileOutputStream in = new FileOutputStream("xanadu.txt");
			in.close();
		} catch (IOException e) {
		}
	}

	public void processk() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new File(""));
			out.println("the text");
		} catch (IOException e) {
			System.out.println("Exception occurred");
		} finally {
			if(true) {
				out.close();	
			}
		}
	}
	
	//resource is not closed anywhere
	public void process2() {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("out.txt", true)));
			out.println("the text. hello world");
		} catch (IOException e) {
			System.out.println("Exception occurred while writing");
			System.out.println("Unexpected IOException");
		}
	}
}
